// Copyright 2019 Christopher Ho
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <gtest/gtest.h>
#include <mpc_controller/mpc_controller.hpp>
#include <motion_testing/motion_testing.hpp>
#include <time_utils/time_utils.hpp>

#include <vector>

using motion::control::controller_common::ControlReference;
using motion::control::mpc_controller::Real;
using motion::control::mpc_controller::StateWeight;
using motion::control::mpc_controller::Config;
using motion::control::mpc_controller::BehaviorConfig;
using motion::control::mpc_controller::LimitsConfig;
using motion::control::mpc_controller::OptimizationConfig;
using motion::control::mpc_controller::VehicleConfig;
using motion::control::mpc_controller::Interpolation;
using motion::control::mpc_controller::MpcController;
using motion::motion_testing::make_state;
using motion::motion_testing::constant_velocity_trajectory;
using motion::motion_testing::constant_acceleration_turn_rate_trajectory;
using motion::control::mpc_controller::State;
using time_utils::from_message;
using time_utils::to_message;

// Default values from:
// http://www.mchenrysoftware.com/medit32/readme/msmac/
// default.htm?turl=examplestirecorneringstiffnesscalculation1.htm
//
// http://www.mchenrysoftware.com/medit32/readme/msmac/
// default.htm?turl=examplestirecorneringstiffnesscalculation1.htmF
//
// http://www.mchenrysoftware.com/forum/Yaw%20Inertia.pdf
class sanity_checks_base : public ::testing::Test
{
protected:
  LimitsConfig limits_cfg_{
    // Debug bounds
    // {-999.1F, 999.0F},
    // {-999.1F, 999.0F},
    // {-999.1F, 999.0F},
    // {-999.1F, 999.0F},
    // {-999.1F, 999.0F},
    // {-999.1F, 999.0F},
    // {-999.1F, 999.0F}
    // Real Bounds
    {0.01F, 35.0F},  // Longitudinal velocity
    {-3.0F, 3.0F},  // Lateral velocity
    {-3.0F, 3.0F},  // Acceleation
    {-3.0F, 3.0F},  // yaw rate
    {-10.0F, 10.0F},  // Jerk
    {-0.331F, 0.331F},  // Steer angle
    {-0.331F, 0.331F}  // Steer angle rate
  };
  VehicleConfig vehicle_cfg_{
    // 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F
    // Parameters from LaValle
    1.2F,  // CG to front
    1.5F,  // CG to rear
    17000.0F,  // front cornering
    20000.0F,  // rear cornering
    1460.0F,  // mass
    2170.0F  // Inertia
    // Paraeters from McHenry
    // 1.5F,  // CG to front
    // 1.5F,  // CG to rear
    // 880.0F,  // front cornering
    // 587.0F,  // rear cornering
    // 1000.0F,  // mass
    // 12500.0F  // Inertia
  };
  BehaviorConfig behavior_cfg_{
    3.0F,  // Stop rate
    std::chrono::milliseconds(100LL),  // time step
    ControlReference::SPATIAL};
  OptimizationConfig opt_cfg_{
    StateWeight{
      10.0F,  // pose
      10.0F,  // heading
      10.0F,  // longitudinal velocity
      10.0F,  // lateral velocity
      10.0F,  // yaw rate
      10.0F,  // acceleration
      10.0F,  // jerk
      10.0F,  // steer angle
      10.0F  // steer angle rate
    },
    StateWeight{
      1000.0F,  // pose
      1000.0F,  // heading
      1000.0F,  // longitudinal velocity
      1000.0F,  // lateral velocity
      1000.0F,  // yaw rate
      1000.0F,  // acceleration
      1000.0F,  // jerk
      1000.0F,  // steer angle
      1000.0F  // steer angle rate
    }
  };
};  // class sanity_checks_base
class sanity_checks : public sanity_checks_base
{
protected:
  MpcController controller_{
    Config{
      limits_cfg_,
      vehicle_cfg_,
      behavior_cfg_,
      opt_cfg_,
      std::chrono::milliseconds(5LL),  // sample_period_tolerance
      std::chrono::milliseconds(100LL),  // control_lookahead_duration
      Interpolation::YES}};
};  // class sanity_checks

class sanity_checks_no_interpolation : public sanity_checks_base
{
protected:
  MpcController controller_{
    Config{
      limits_cfg_,
      vehicle_cfg_,
      behavior_cfg_,
      opt_cfg_,
      std::chrono::milliseconds(5LL),  // sample_period_tolerance
      std::chrono::milliseconds(100LL),  // control_lookahead_duration
      Interpolation::NO}};
};  // class sanity_checks_no_interpolation

TEST_F(sanity_checks_no_interpolation, bad_trajectory_sample_interval)
{
  const auto dt = controller_.get_config().behavior().time_step();
  const auto traj_ = constant_velocity_trajectory(0.0F, 0.0F, 0.0F, 0.0F, dt);
  const auto ns = std::chrono::nanoseconds(1LL);
  const auto dt_ = controller_.get_config().sample_period_tolerance();
  const std::vector<decltype(traj_.points)::size_type> inds{0U, 10U, traj_.points.size() - 1U};
  for (auto idx : inds) {
    {
      auto traj = traj_;
      traj.points[idx].time_from_start = to_message((idx * dt) + (dt_ + ns));
      EXPECT_THROW(controller_.set_trajectory(traj), std::domain_error);
      traj.points[idx].time_from_start = to_message((idx * dt) + (dt_));
      EXPECT_NO_THROW(controller_.set_trajectory(traj));
    }
    {
      auto traj = traj_;
      traj.points[idx].time_from_start = to_message((idx * dt) - (dt_ + ns));
      EXPECT_THROW(controller_.set_trajectory(traj), std::domain_error);
      traj.points[idx].time_from_start = to_message((idx * dt) - (dt_));
      EXPECT_NO_THROW(controller_.set_trajectory(traj));
    }
  }
}

struct ConstantParam
{
  Real x0;
  Real y0;
  Real heading;
  Real v0;
  Real a0;
  Real omega0;
  std::chrono::nanoseconds dt;  // if zero, use configuration dt
};  // struct ConstantParam

constexpr auto zero_ns = std::chrono::nanoseconds::zero();

class sanity_checks_oneshot
  : public sanity_checks, public testing::WithParamInterface<ConstantParam>
{
};
class sanity_checks_simulation
  : public sanity_checks, public testing::WithParamInterface<ConstantParam>
{
};

// Same velocity on a constant velocity track should result in no acceleration
TEST_P(sanity_checks_oneshot, constant_trajectory)
{
  const auto p = GetParam();
  auto dt = p.dt;
  if (std::chrono::nanoseconds::zero() == dt) {
    dt = controller_.get_config().behavior().time_step();
  }
  const auto traj =
    constant_acceleration_turn_rate_trajectory(p.x0, p.y0, p.heading, p.v0, p.a0, p.omega0, dt);
  controller_.set_trajectory(traj);
  const auto state =
    make_state(p.x0, p.y0, p.heading, p.v0, p.a0, 0.0F, from_message(traj.header.stamp));
  const auto cmd = controller_.compute_command(state);
  constexpr float TOL = 1.0E-3F;  // numerical algorithms are inexact
  EXPECT_LT(std::fabs(cmd.long_accel_mps2 - (p.a0)), TOL);
  EXPECT_LT(std::fabs(cmd.front_wheel_angle_rad - (0.0F)), TOL);
  EXPECT_LT(std::fabs(cmd.rear_wheel_angle_rad - (0.0F)), TOL);
  EXPECT_EQ(cmd.stamp, state.header.stamp);
  const auto cmd_dot = controller_.get_computed_control_derivatives();
  ASSERT_LT(std::fabs(cmd_dot.jerk_mps3 - (0.0F)), TOL);
  ASSERT_LT(std::fabs(cmd_dot.steer_angle_rate_rps - (0.0F)), TOL);
  if (HasFailure()) {
    controller_.debug_print(std::cout);
  }
}

INSTANTIATE_TEST_CASE_P(
  oneshot,
  sanity_checks_oneshot,
  testing::Values(
    // Different acceleration (profiles)
    ConstantParam{0.0F, 0.0F, 0.0F, 10.0F, 0.0F, 0.0F, zero_ns},
    ConstantParam{0.0F, 0.0F, 0.0F, 3.0F, 1.0F, 0.0F, zero_ns},
    ConstantParam{0.0F, 0.0F, 0.0F, 15.0F, -1.0F, 0.0F, zero_ns},
    // Different angles
    ConstantParam{0.0F, 0.0F, 2.0F, 3.0F, 0.0F, 0.0F, zero_ns},
    ConstantParam{0.0F, 0.0F, -2.0F, 3.0F, 0.0F, 0.0F, zero_ns},
    ConstantParam{0.0F, 0.0F, 3.14F, 3.0F, 0.0F, 0.0F, zero_ns},
    ConstantParam{0.0F, 0.0F, -3.14F, 3.0F, 0.0F, 0.0F, zero_ns},
    // Different offsets
    ConstantParam{3.0F, 0.0F, 0.0F, 10.0F, 0.0F, 0.0F, zero_ns},
    ConstantParam{0.0F, -30.0F, 0.0F, 3.0F, 1.0F, 0.0F, zero_ns},
    ConstantParam{-15.0F, 95.0F, 0.0F, 15.0F, -1.0F, 0.0F, zero_ns},
    // Different sample periods
    ConstantParam{0.0F, 0.0F, 0.0F, 10.0F, 0.0F, 0.0F, std::chrono::milliseconds{60LL}},
    ConstantParam{0.0F, 0.0F, 0.0F, 10.0F, 0.0F, 0.0F, std::chrono::milliseconds{160LL}}
));

// Lateral offset should be rejected by inducing some steering
TEST_F(sanity_checks, lateral_offset)
{
  const auto x0 = 0.0F;
  const auto y0 = 0.0F;
  const auto heading = 0.0F;
  const auto v0 = 10.0F;
  const auto dt = std::chrono::milliseconds(100LL);
  const auto traj = constant_velocity_trajectory(x0, y0, heading, v0, dt);
  controller_.set_trajectory(traj);
  const auto state =
    make_state(x0, y0 - 3.0F, heading, v0, 0.0F, 0.0F, from_message(traj.header.stamp));
  const auto cmd = controller_.compute_command(state);
  constexpr float TOL = 1.0E-4F;  // numerical algorithms are inexact
  EXPECT_LT(std::fabs(cmd.long_accel_mps2 - (0.0F)), TOL);
  EXPECT_GT(cmd.front_wheel_angle_rad, 0.0F);
  EXPECT_LT(std::fabs(cmd.rear_wheel_angle_rad - (0.0F)), TOL);
  EXPECT_EQ(cmd.stamp, state.header.stamp);
  // No guarantee of zero derivatives
  // const auto cmd_dot = controller_.get_computed_control_derivatives();
  // ASSERT_LT(std::fabs(cmd_dot.jerk_mps3 - (0.0F)), TOL);
  // ASSERT_GT(cmd_dot.steer_angle_rate_rps, 0.0F);
  if (HasFailure()) {
    controller_.debug_print(std::cout);
  }
}

// Fake simulation: should be able to follow trajectory from start to finish without going nuts
TEST_P(sanity_checks_simulation, constant_trajectory_simulation)
{
  const auto p = GetParam();
  auto dt = p.dt;
  auto stop_indices = 2U;
  if (std::chrono::nanoseconds::zero() == dt) {
    dt = controller_.get_config().behavior().time_step();
  }
  // Dumb hard coding
  if (std::chrono::milliseconds(60) == dt) {
    stop_indices = 3U;
  }
  const auto traj =
    constant_acceleration_turn_rate_trajectory(p.x0, p.y0, p.heading, p.v0, p.a0, p.omega0, dt);
  controller_.set_trajectory(traj);
  State state;
  auto iters = traj.points.size();
  // More dumb hard coding: I think there's some accumulation of numerical errors due to warm
  // starting and interpolation
  if (std::chrono::milliseconds(160) == dt) {
    iters = 35U;
  }
  for (auto idx = 0U; idx < iters; ++idx) {
    const auto pt = traj.points[idx];
    state.state = pt;
    state.header.stamp =
      to_message(from_message(traj.header.stamp) + from_message(pt.time_from_start));
    const auto cmd = controller_.compute_command(state);

    // numerical algorithms are inexact
    const float TOL = std::fabs(p.omega0) < 1.0E-5F ? 1.0E-3F : 1.0E-1F;
    // ^^ loose tolerance for non-straight cases because IDK what's happening
    if (idx < traj.points.size() - stop_indices) {
      EXPECT_LT(std::fabs(cmd.long_accel_mps2 - (p.a0)), TOL) << idx;
      EXPECT_LT(std::fabs(cmd.front_wheel_angle_rad - (0.0F)), TOL) << idx;
      EXPECT_LT(std::fabs(cmd.rear_wheel_angle_rad - (0.0F)), TOL) << idx;
      EXPECT_EQ(cmd.stamp, state.header.stamp) << idx;
      const auto cmd_dot = controller_.get_computed_control_derivatives();
      ASSERT_LT(std::fabs(cmd_dot.jerk_mps3 - (0.0F)), TOL);
      ASSERT_LT(std::fabs(cmd_dot.steer_angle_rate_rps - (0.0F)), TOL);
    } else {
      // Safe stop
      const auto safe_decel_rate = controller_.get_config().behavior().safe_deceleration_rate();
      EXPECT_LT(std::fabs(cmd.long_accel_mps2 - (-safe_decel_rate)), TOL) << idx;
      EXPECT_LT(std::fabs(cmd.front_wheel_angle_rad - (0.0F)), TOL) << idx;
      EXPECT_LT(std::fabs(cmd.rear_wheel_angle_rad - (0.0F)), TOL) << idx;
      EXPECT_EQ(cmd.stamp, state.header.stamp) << idx;
      const auto cmd_dot = controller_.get_computed_control_derivatives();
      ASSERT_LT(std::fabs(cmd_dot.jerk_mps3 - (0.0F)), TOL);
      ASSERT_LT(std::fabs(cmd_dot.steer_angle_rate_rps - (0.0F)), TOL);
    }
    if (HasFailure()) {
      controller_.debug_print(std::cout);
      ASSERT_TRUE(false);
    }
  }
}

INSTANTIATE_TEST_CASE_P(
  simulation,
  sanity_checks_simulation,
  testing::Values(
    ConstantParam{0.0F, 0.0F, 0.0F, 10.0F, 0.0F, 0.0F, zero_ns},
    ConstantParam{10.0F, 5.0F, 0.0F, 3.0F, 1.0F, 0.0F, zero_ns},
    ConstantParam{-50.0F, -10.0F, 2.0F, 7.0F, 0.2F, 0.0F, zero_ns},
    ConstantParam{17.0F, -13.0F, -2.0F, 15.0F, -1.0F, 0.0F, zero_ns},
    // Different sample periods
    ConstantParam{0.0F, 0.0F, 0.0F, 10.0F, 0.0F, 0.0F, std::chrono::milliseconds{60LL}},
    ConstantParam{0.0F, 0.0F, 0.0F, 10.0F, 0.0F, 0.0F, std::chrono::milliseconds{160LL}},
    // Turn past +/- pi cases
    ConstantParam{0.0F, 0.0F, -3.14F, 10.0F, 0.0F, -0.01F, zero_ns},
    ConstantParam{0.0F, 0.0F, 3.14F, 10.0F, 0.0F, 0.01F, zero_ns}
));
